//
//  ApiConfig.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import Foundation
import UIKit

struct ApiConfig {
    
    #if DEBUG
    
    static private let url = "http://dev1.tamrontech.com/akoneya"
    
    #else
    
    static private let url = "https://api.akoneya.com/akoneya"
    
    #endif

    static let successCodeRange = 200...300
    
    static let NullState = "null"
    static var baseUrl: String {
        return url
    }
    
    enum FilterKey : String{
        case data = "data"
        case result = "result"
        
        func getString() -> String {
            return self.rawValue
        }
    }
    
    enum MIMEType: String {
        case image = "image"
        case video = "video"
    }
    
    enum Privacy: String {
        case publicPriv = "public"
        case privatePriv = "private"
    }
}
