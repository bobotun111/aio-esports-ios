//
//  ApiClient.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import Foundation
import Alamofire
import Reachability
import SystemConfiguration
import SwiftyJSON
import RxSwift
import RxCocoa
import Combine

class ApiClient {
    //MARK:- NETWORK CALLS
    static let shared = ApiClient()
    private let APIManager: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.urlCache = nil
        configuration.requestCachePolicy = .reloadIgnoringCacheData
        let delegate = Session.default.delegate
        let manager = Session.init(configuration: configuration,
                                   delegate: delegate)
        return manager
    }()
    
    
    public func upload(url : String,
                       method : HTTPMethod = .post,
                       parameters : Parameters = [:],
                       headers : HTTPHeaders = [:],
                       dataList: [String: Data] = [:]) -> Observable<Any> {
        
        if !ApiClient.checkReachable() {
            return Observable.create { observer in
                observer.onError(ErrorType.NoInterntError)
                return Disposables.create()
            }
        }
        
        var headers = headers
        headers["Content-Type"] = "application/json"
        headers["Content-Language"] = LocalizationManager.sharedInstance.getLocale().getContentLanguage()
        
        return Observable.create { observer in
            self.APIManager.upload(multipartFormData: { multipartFormData in
                
                print("Parameter :::: ", parameters)
                for (key, value) in parameters {
                    
                    if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue ) {
                        multipartFormData.append(data, withName: key)
                    }
                } //Optional for extra parameters
                
                for (key, value) in dataList {
                    multipartFormData.append(value, withName: key, fileName: "\(key).jpg", mimeType: "image/jpg")
                }
                
            },
            to: url, method: .post,
            headers: headers).responseJSON { (response) in
                switch response.result {
                case .success:
                    
                    let responseJson = JSON(response.data as Any)
                    let result : String = responseJson["result"].rawString() ?? ""
                    
                    /* result = Not Null **/
                    if let data = response.data, result != ApiConfig.NullState {
                        
                        observer.onNext(data)
                        observer.onCompleted()
                    }
                    /* result = null **/
                    else if let data = response.data, let responseJSON = data.decode(modelType: BaseResponse.self),
                            let message: String = responseJSON.message, result == ApiConfig.NullState,
                            let statusCode = response.response?.statusCode {
                        let successRange = 200...300
                        
                        if successRange.contains(statusCode) {
                            observer.onNext(data)
                            observer.onCompleted()
                        }else{
                            let error = ErrorType.KnownError(message)
                            observer.onError(error)
                        }
                        
                    }
                case .failure:
                    
                    if let statusCode = response.response?.statusCode , statusCode == 401 {
                        let responseJson = JSON(response.data as Any)
                        if let status_code = responseJson["statusCode"].int {
                            
                            /* statusCode == 40101 => Unauthorized
                             statusCode == 40102 => Token Expire
                             statusCode == 40103 => Can't refresh token
                             */
                            if status_code == 40101 || status_code == 40102 || status_code == 40103 || status_code == 401 {
                                let error = ErrorType.TokenExpireError(status_code)
                                observer.onError(error)
                            }
                        }
                    }
                    else{
                        let responseJson = JSON(response.data as Any)
                        let result : String = responseJson["result"].rawString() ?? ""
                        
                        
                        if let data = response.data, let responseJSON = data.decode(modelType: BaseResponse.self), let message: String = responseJSON.message,
                           result == ApiConfig.NullState  {
                            
                            let error = ErrorType.KnownError(message)
                            observer.onError(error)
                        }else{
                            let error = ErrorType.UnKnownError
                            observer.onError(error)
                        }
                    }
                    
                }
            }
            
            return Disposables.create()
        }
        
    }
    
    public func request(url : String,
                        method : HTTPMethod = .get,
                        parameters : Parameters = [:],
                        headers : HTTPHeaders = [:] ,
                        isOpenAPI : Bool = false) -> Observable<Any>{
        
        if !ApiClient.checkReachable() {
            return Observable.create { observer in
                observer.onError(ErrorType.NoInterntError)
                return Disposables.create()
            }
        }
        
        var headers = headers
        headers["Content-Type"] = "application/json"
        headers["Content-Language"] = LocalizationManager.sharedInstance.getLocale().getContentLanguage()
        
        let encoding : ParameterEncoding = (method == .get ? URLEncoding.default : JSONEncoding.default)
        
        return Observable.create { observer in
            self.APIManager.request(url, method: method, parameters: parameters,encoding: encoding, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    
                    let responseJson = JSON(response.data as Any)
                    let result : String = responseJson["result"].rawString() ?? ""
                    
                    /* result = Not Null **/
                    if let data = response.data, result != ApiConfig.NullState {
                        
                        observer.onNext(data)
                        observer.onCompleted()
                    }
                    /* result = null **/
                    else if let data = response.data, let responseJSON = data.decode(modelType: BaseResponse.self),
                            let message: String = responseJSON.message, result == ApiConfig.NullState,
                            let statusCode = response.response?.statusCode {
                        let successRange = 200...300
                        
                        if successRange.contains(statusCode) {
                            observer.onNext(data)
                            observer.onCompleted()
                        }else{
                            let error = ErrorType.KnownError(message)
                            observer.onError(error)
                        }
                        
                    }
                case .failure:
                    
                    if let statusCode = response.response?.statusCode , statusCode == 401 {
                        let responseJson = JSON(response.data as Any)
                        if let status_code = responseJson["statusCode"].int {
                            
                            /* statusCode == 40101 => Unauthorized
                             statusCode == 40102 => Token Expire
                             statusCode == 40103 => Can't refresh token
                             */
                            if status_code == 40101 || status_code == 40102 || status_code == 40103 || status_code == 401 {
                                let error = ErrorType.TokenExpireError(status_code)
                                observer.onError(error)
                            }
                        }
                    }
                    else{
                        let responseJson = JSON(response.data as Any)
                        let result : String = responseJson["result"].rawString() ?? ""
                        
                        
                        if let data = response.data, let responseJSON = data.decode(modelType: BaseResponse.self), let message: String = responseJSON.message,
                           result == ApiConfig.NullState  {
                            
                            let error = ErrorType.KnownError(message)
                            observer.onError(error)
                        }else{
                            let error = ErrorType.UnKnownError
                            observer.onError(error)
                        }
                    }
                    
                }
            }
            return Disposables.create()
        }
    }
    
    public func uploadCombine(url : String,
                              method : HTTPMethod = .post,
                              parameters : Parameters? = [:],
                              headers : HTTPHeaders = [:],
                              dataList: [String: Data]? = [:]) -> AnyPublisher<Data,Error> {
        
        if !ApiClient.checkReachable() {
            return Future<Data,Error> { promise in
                promise(.failure(ErrorType.NoInterntError))
            }.eraseToAnyPublisher()
        }
        
        var headers = headers
        headers["Content-Type"] = "application/json"
        
        return Future<Data,Error>{ promise in
            self.APIManager.upload(multipartFormData: { multipartFormData in
                
                parameters?.forEach { (key, value) in
                    if let data = "\(value)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue) ) {
                        multipartFormData.append(data, withName: key)
                    }
                }
                //Optional for extra parameters
                
                dataList?.forEach { (key, value) in
                    multipartFormData.append(value, withName: key, fileName: "\(key).jpg", mimeType: "image/jpg")
                }
                
            },
            to: url, method: .post,
            headers: headers).responseJSON { (response) in
                switch response.result {
                case .success:
                    
                    let responseJson = JSON(response.data as Any)
                    let result : String = responseJson["result"].rawString() ?? ""
                    
                    /* result = Not Null **/
                    if let data = response.data, result != ApiConfig.NullState {
                        promise(.success(data))
                    }
                    /* result = null **/
                    else if let data = response.data, let responseJSON = data.decode(modelType: BaseResponse.self),
                            let message: String = responseJSON.message, result == ApiConfig.NullState,
                            let statusCode = response.response?.statusCode {
                        let successRange = 200...300
                        
                        if successRange.contains(statusCode) {
                            promise(.success(data))
                        }else{
                            let error = ErrorType.KnownError(message)
                            promise(.failure(error))
                        }
                        
                    }
                case .failure:
                    
                    if let statusCode = response.response?.statusCode , statusCode == 401 {
                        let responseJson = JSON(response.data as Any)
                        if let status_code = responseJson["statusCode"].int {
                            
                            /* statusCode == 40101 => Unauthorized
                             statusCode == 40102 => Token Expire
                             statusCode == 40103 => Can't refresh token
                             */
                            if status_code == 40101 || status_code == 40102 || status_code == 40103 || status_code == 401 {
                                let error = ErrorType.TokenExpireError(status_code)
                                promise(.failure(error))
                            }
                        }
                    }
                    else{
                        let responseJson = JSON(response.data as Any)
                        let result : String = responseJson["result"].rawString() ?? ""
                        
                        
                        if let data = response.data, let responseJSON = data.decode(modelType: BaseResponse.self), let message: String = responseJSON.message,
                           result == ApiConfig.NullState  {
                            
                            let error = ErrorType.KnownError(message)
                            promise(.failure(error))
                        }else{
                            let error = ErrorType.UnKnownError
                            promise(.failure(error))
                        }
                    }
                    
                }
            }
        }.eraseToAnyPublisher()
        
    }
    
    public func requestCombine(url : String,
                               method : HTTPMethod = .get,
                               parameters : Parameters = [:],
                               headers : HTTPHeaders = [:] ,
                               isOpenAPI : Bool = false,
                               forceLanguage : LanguageType? = nil) -> AnyPublisher<Data,Error> {
        
        if !ApiClient.checkReachable() {
            return Future<Data,Error> { promise in
                promise(.failure(ErrorType.NoInterntError))
            }.eraseToAnyPublisher()
        }
        
        var headers = headers
        headers["Content-Type"] = "application/json"
        
        headers["Content-Language"] = (forceLanguage == nil) ?  LocalizationManager.sharedInstance.getLocale().getContentLanguage() : forceLanguage?.getContentLanguage()
        let encoding : ParameterEncoding = (method == .get ? URLEncoding.default : JSONEncoding.default)
        
        return Future<Data,Error>{ promise in
            
            self.APIManager.request(url, method: method, parameters: parameters,encoding: encoding, headers: headers).validate().responseJSON { (response) in
                switch response.result {
                case .success:
                    
                    let responseJson = JSON(response.data as Any)
                    let result : String = responseJson["result"].rawString() ?? ""
                    
                    /* result = Not Null **/
                    if let data = response.data, result != ApiConfig.NullState {
                        promise(.success(data))
                    }
                    /* result = null **/
                    else if let data = response.data, let responseJSON = data.decode(modelType: BaseResponse.self),
                            let message: String = responseJSON.message, result == ApiConfig.NullState,
                            let statusCode = response.response?.statusCode {
                        let successRange = 200...300
                        
                        if successRange.contains(statusCode) {
                            promise(.success(data))
                        }else{
                            let error = ErrorType.KnownError(message)
                            promise(.failure(error))
                        }
                        
                    }
                case .failure:
                    
                    if let statusCode = response.response?.statusCode , statusCode == 401 {
                        let responseJson = JSON(response.data as Any)
                        if let status_code = responseJson["statusCode"].int {
                            
                            /* statusCode == 40101 => Unauthorized
                             statusCode == 40102 => Token Expire
                             statusCode == 40103 => Can't refresh token
                             */
                            if status_code == 40101 || status_code == 40102 || status_code == 40103 || status_code == 401 {
                                let error = ErrorType.TokenExpireError(status_code)
                                promise(.failure(error))
                            }
                        }
                    }
                    else{
                        let responseJson = JSON(response.data as Any)
                        let result : String = responseJson["result"].rawString() ?? ""
                        
                        
                        if let data = response.data, let responseJSON = data.decode(modelType: BaseResponse.self), let message: String = responseJSON.message,
                           result == ApiConfig.NullState  {
                            
                            let error = ErrorType.KnownError(message)
                            promise(.failure(error))
                        }else{
                            let error = ErrorType.UnKnownError
                            promise(.failure(error))
                        }
                    }
                    
                }
            }
            
        }.eraseToAnyPublisher()
        
    }
    
    
}

//MARK:- CHECK NETWORK
extension ApiClient {
    
    static func isOnline(callback: @escaping (Bool) -> Void){
        //declare this property where it won't go out of scope relative to your listener
        
        let reachability = try! Reachability()
        
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
                callback(true)
            } else {
                print("Reachable via Cellular")
                callback(true)
            }
        }
        
        reachability.whenUnreachable = { _ in
            print("Not reachable")
            callback(false)
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
            callback(false)
        }
    }
    
    static func checkReachable() -> Bool{
        let reachability = SCNetworkReachabilityCreateWithName(nil, "www.raywenderlich.com")
        
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability!, &flags)
        
        if (isNetworkReachable(with: flags))
        {
            if flags.contains(.isWWAN) {
                return true
            }
                        
            return true
        }
        else if (!isNetworkReachable(with: flags)) {
            return false
        }
        
        return false
    }
    
    static func checkReachable(success : @escaping () -> Void,
                               failure : @escaping () -> Void){
        
        if checkReachable() {
            success()
        }else{
            failure()
        }
        
    }
    
    static func isNetworkReachable(with flags: SCNetworkReachabilityFlags) -> Bool {
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        let canConnectAutomatically = flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)
        let canConnectWithoutUserInteraction = canConnectAutomatically && !flags.contains(.interventionRequired)
        return isReachable && (!needsConnection || canConnectWithoutUserInteraction)
    }
    
}
