//
//  ErrorType.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import Foundation

enum ErrorType: Error {
    case NoInterntError
    case NoDataError
    case KnownError(_ errorMessage: String)
    case UnKnownError
    case TokenExpireError(_ code : Int)
}
