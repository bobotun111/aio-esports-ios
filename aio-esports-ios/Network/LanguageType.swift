//
//  LanguageType.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import UIKit


enum LanguageType: String {
    case english = "en"
    case burmese = "my-MM"
    
    func getTitle() -> String {
        switch self {
        case .english:
            return LocalizedKey.Language.english.localized()
        case .burmese:
            return LocalizedKey.Language.burmese.localized()
        }
    }
    
    func getContentLanguage() -> String {
        switch self {
        case .english :
            return "en"
        case .burmese :
            return "mm"
        }
    }
}

