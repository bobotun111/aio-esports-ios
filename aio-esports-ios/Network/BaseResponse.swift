//
//  BaseResponse.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import Foundation

struct BaseResponse : Codable {
    
    let statusCode : Int?
    let message : String?
    let status : Bool?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case message = "message"
        case status
    }
    
}

