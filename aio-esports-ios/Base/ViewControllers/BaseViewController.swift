//
//  BaseViewController.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import UIKit
import RxSwift
import RxCocoa
import Combine
import SnapKit

class BaseViewController: UIViewController {
    
    var bindings = Set<AnyCancellable>()
    
    var disposableBag = DisposeBag()
    
    var errorHandlerView : ErrorHandlerView?
    var view_model = BaseViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupLanguage()
        setNavigationColor()
        bindViewModel()
        bindData()
        addTapGestures()
        setupTest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppScreens.shared.currentVC = self
        checkViewControllerAndShowHideTabBar(vc: self)
        checkViewControllerAndAddBackBtn(vc: self)
    }
    
    func removeNavigationBorder(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func checkViewControllerAndShowHideTabBar(vc : UIViewController) {
        let isTop : Bool = isTopViewController(vc: vc)
        isHiddenTabBar(isHidden: !isTop)
    }
    
    func checkViewControllerAndAddBackBtn(vc : UIViewController) {
        if !isTopViewController(vc: vc){
            addBackButton()
        }
    }
    
    func isTopViewController(vc : UIViewController) -> Bool {
        return navigationController?.children.first == vc
    }
    
    func showNavigationBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func hideNavigationBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setupUI(){
        isHiddenTabBar(isHidden: !self.isTopViewController(vc: self))
    }
    
    func setupLanguage(){
        
    }
    
    func bindViewModel() {
        view_model.updatedRefreshTokenPublisher.sink {
            if let response = $0 {
                print("Saved Token >>>>>>>>" , response.message ?? "")
            }
        }.store(in: &bindings)
    }
    
    func bindData() {
        
    }
    
    func reloadScreen() {
        setupUI()
        setNavigationColor()
    }
    
    func setupTest(){
        
    }
    
    func setNavigationColor(){
        if let navigationBar = self.navigationController?.navigationBar {
            let navigationLayer = CALayer()
            var bounds = navigationBar.bounds
            bounds.size.height += view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
            navigationLayer.frame = bounds
            navigationLayer.backgroundColor = UIColor.navigationBarColor.cgColor
            
            if let image = getImageFrom(layer: navigationLayer) {
                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
            }
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get{
            return .portrait
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func isHiddenTabBar(isHidden : Bool) {
        self.tabBarController?.tabBar.isHidden = isHidden
    }
    
    func addTapGestures() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureRecognizer:)))
        tap.cancelsTouchesInView = false
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
    }
    
    func removeAllPerviousControllers(controller: AnyClass) {
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            return !vc.isKind(of: controller) && !vc.isKind(of: controller)
        })
    }
    
    @objc func handleTap(gestureRecognizer: UIGestureRecognizer) {
        self.view.endEditing(true)
        willDismissKeyBoard()
    }
    
    func showNoInternetConnectionAlert(){
        AlertManager.showCommonError(message: LocalizedKey.ToastAndPickerMsg.noInternet.localized(), vc: self)
    }
    
    func willDismissKeyBoard() {
        
    }
    
    func isShowNoDataAndInternet(isShow : Bool , isServerError : Bool = false, errorImage: UIImage? = nil, errorTitle: String? = nil, errorDesc: String? = nil) {
        errorHandlerView?.removeFromSuperview()
        errorHandlerView = ErrorHandlerView(frame: self.view.frame)
        errorHandlerView?.translatesAutoresizingMaskIntoConstraints = false
        errorHandlerView?.delegate = self
        if isShow {
            errorHandlerView?.setupView(isShow: isShow, isServerError: isServerError, errorImage: errorImage, errorTitle: errorTitle, errorDesc: errorDesc)
            self.view.addSubview(errorHandlerView!)
            errorHandlerView?.snp.makeConstraints({ (errorView) in
                errorView.left.equalToSuperview()
                errorView.right.equalToSuperview()
                errorView.centerY.equalToSuperview()
            })
            
        } else {
            errorHandlerView?.removeView()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "FCMToken"), object: nil)
    }
    
}

extension BaseViewController : ErrorHandlerDelegate {
    func didTapRetry() {
        
        ApiClient.checkReachable(success: {
            self.isShowNoDataAndInternet(isShow: false)
            self.reloadScreen()
        }) {
            //not reachable
            self.showNoInternetConnectionToast()
            self.isShowNoDataAndInternet(isShow: true)
        }
        
    }
}

extension BaseViewController {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        touch.view == self.view
    }
}

