//
//  BaseView.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import UIKit
import RxCocoa
import RxSwift
import Combine

class BaseView : UIView {
    
    var disposableBag = DisposeBag()
    
    var view: UIView!
    
    var bindings = Set<AnyCancellable>()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        setupTest()
        setupLanguae()
        bindData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
        setupTest()
        setupLanguae()
        bindData()
    }
    
    func setupLanguae(){
        
    }
    
    func bindData(){
        
    }
    
    func setupUI() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView! {
        return UINib(nibName: String(describing: type(of: self)), bundle: nil).instantiate(withOwner: self, options: nil).first as? UIView ?? UIView(frame: CGRect.zero)
    }
    
    func setupTest() {
        
    }

}

