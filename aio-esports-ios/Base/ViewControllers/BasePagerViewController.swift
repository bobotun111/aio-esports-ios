//
//  BasePagerViewController.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import Foundation
import XLPagerTabStrip
import RxSwift
import RxCocoa
import Combine

class BasePagerViewController: ButtonBarPagerTabStripViewController {
    
    let disposableBag = DisposeBag()
    var bindings = Set<AnyCancellable>()
   
    var vcList : [UIViewController]?
    var errorHandlerView : ErrorHandlerView?
    
    override func viewDidLoad (){
        setupUI()
        super.viewDidLoad()
        bindViewModel()
        bindData()
        setupTest()
    }
    
    func setupUI() {
        navigationItem.largeTitleDisplayMode = .never
        addBackButton()
    }
    
    func bindViewModel() {
        
    }
    
    func bindData () {
        
    }
    
    func setupTest() {
        
    }
    
    func reloadScreen()  {
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.children[0].beginAppearanceTransition(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.children[0].endAppearanceTransition()
    }
    
    func isShowNoDataAndInternet(isShow : Bool , isServerError : Bool = false) {
        errorHandlerView?.removeFromSuperview()
        errorHandlerView = ErrorHandlerView(frame: self.view.frame)
        errorHandlerView?.translatesAutoresizingMaskIntoConstraints = false
        errorHandlerView?.delegate = self
        if isShow {
            errorHandlerView?.setupView(isShow: isShow, isServerError: isServerError)
            self.view.addSubview(errorHandlerView!)
            errorHandlerView?.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
            errorHandlerView?.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            
        }
        else {
            errorHandlerView?.removeView()
        }
        
    }
    
}

extension BasePagerViewController : ErrorHandlerDelegate {
    func didTapRetry() {
        ApiClient.checkReachable(success: {
            self.isShowNoDataAndInternet(isShow: false)
            self.reloadScreen()
        }) {
            //not reachable
            self.isShowNoDataAndInternet(isShow: true)
        }
        
    }
}

