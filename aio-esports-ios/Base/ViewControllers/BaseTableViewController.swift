//
//  BaseTableViewController.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import UIKit
import RxCocoa
import RxSwift
import CRRefresh

enum DragDirection {
    case Up
    case Down
}

var topViewInitialHeight : CGFloat = 380
let topViewFinalHeight : CGFloat = 0
let topViewHeightConstraintRange = topViewFinalHeight..<topViewInitialHeight
typealias TableViewDelegateAndDataSource = UITableViewDelegate & UITableViewDataSource
typealias CollectionViewDelegateAndDataSource = UICollectionViewDelegate & UICollectionViewDataSource

protocol InnerTableViewScrollDelegate: AnyObject {
    
    var currentHeaderHeight: CGFloat { get }
    func innerTableViewDidScroll(withDistance scrollDistance: CGFloat)
    func innerTableViewScrollEnded(withScrollDirection scrollDirection: DragDirection)
    
}

class BaseTableViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintLeadingTableview: NSLayoutConstraint!
    @IBOutlet weak var constraintTrailingTableView: NSLayoutConstraint!
    
    @IBOutlet weak var footerStickyView: UIView!
    @IBOutlet weak var heightFooterStickyView: NSLayoutConstraint!
    
    @IBOutlet weak var headerStickyView: UIView!
    @IBOutlet weak var heightHeaderStickyView: NSLayoutConstraint!
    
    fileprivate let activityIndicator = UIActivityIndicatorView()
    
    let showPullToRefreshAnimationViewPublishRelay = PublishRelay<Bool>()
    let hidePullToRefreshAnimationViewPublishRelay = PublishRelay<Bool>()
    let showFooterLoadingViewPublishRealy = PublishRelay<Bool>()
    let hideFooterLoadingViewPublishRealy = PublishRelay<Bool>()
    let isNoMoreDataPublishRelay = PublishRelay<Bool>()
    // for segmentControl
    weak var innerTableViewScrollDelegate: InnerTableViewScrollDelegate?
    
    private var dragDirection: DragDirection = .Up
    private var oldContentOffset = CGPoint.zero
   
   
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func bindData() {
        super.bindData()
        
        hidePullToRefreshAnimationViewPublishRelay.bind{
            if $0 {
                self.tableView.cr.endHeaderRefresh()
            }
        }.disposed(by: disposableBag)
        
        hideFooterLoadingViewPublishRealy.bind{
            if $0 {
                self.tableView.cr.endLoadingMore()
            }
        }.disposed(by: disposableBag)
        
        isNoMoreDataPublishRelay.bind {
                self.tableView.cr.footer?.isHidden = $0
        }.disposed(by: disposableBag)
    }
    
    override func setupUI() {
        super.setupUI()
        self.removeNavigationBorder()
        setupTableView()
       
    }
    
    func setupTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
    }
    
    func setupHeaderFooterLoadingAnimation(){
        setupHeaderLoadingAnimation()
        setupFooterLoadingAnimation()
    }
    
    func setupHeaderLoadingAnimation(){
        tableView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            self?.showPullToRefreshAnimationViewPublishRelay.accept(true)
        }
    }
    
    func setupFooterLoadingAnimation(){
        tableView.cr.addFootRefresh(animator: NormalFooterAnimator()) { [weak self] in
            self?.showFooterLoadingViewPublishRealy.accept(true)
        }
    }
    
    func registerForCells(identifiers : [String]){
        tableView.registerForCells(cells: identifiers)
    }
    
    func getTabbarItemTitle() -> String {
        return tabBarItem.title ?? "Title"
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get{
            return .portrait
        }
    }
    
    //MARK:- ActivityIndicator
    /* for pull to refresh and load more*/
    func showBottomIndicator(){
        activityIndicator.startAnimating()
        activityIndicator.color = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        activityIndicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0)
            , width: tableView.bounds.width, height: CGFloat(44))
        
        self.tableView.tableFooterView = activityIndicator
    }
    
    func setupConstriantForTabletSize(leading: CGFloat, trailing: CGFloat){
        if iOSDeviceSizes.tabletSize.getBool() {
            constraintLeadingTableview.constant = leading
            constraintTrailingTableView.constant = trailing
        }
    }
    
    func hideBottomIndicator(){
        activityIndicator.stopAnimating()
        self.tableView.tableFooterView = UIView()
    }
    
    func showTopIndicator(){
        activityIndicator.startAnimating()
        activityIndicator.color = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        activityIndicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0)
            , width: tableView.bounds.width, height: CGFloat(44))
        
        self.tableView.tableHeaderView = activityIndicator
    }
    
    func hideTopIndicator(){
        activityIndicator.stopAnimating()
        self.tableView.tableHeaderView = UIView()
    }
    
    func scrollToBottom(topInset: CGFloat = 0, isAlreadyCalled: Bool = false) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            var rect = self.tableView.bounds
            rect.origin.y = max(self.tableView.contentSize.height - self.tableView.bounds.size.height, topInset)
            self.tableView.scrollRectToVisible(rect, animated: false)
            if !isAlreadyCalled {
                self.scrollToBottom(topInset: topInset, isAlreadyCalled: true)
            }
        }
    }
}

//MARK:- Loading Indicator
extension BaseTableViewController : TableViewDelegateAndDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let delta = scrollView.contentOffset.y - oldContentOffset.y
        
        let topViewCurrentHeightConst = innerTableViewScrollDelegate?.currentHeaderHeight
        
        if let topViewUnwrappedHeight = topViewCurrentHeightConst {
            
            /**
             *  Re-size (Shrink) the top view only when the conditions meet:-
             *  1. The current offset of the table view should be greater than the previous offset indicating an upward scroll.
             *  2. The top view's height should be within its minimum height.
             *  3. Optional - Collapse the header view only when the table view's edge is below the above view - This case will occur if you are using Step 2 of the next condition and have a refresh control in the table view.
             */
            
            if delta > 0,
                topViewUnwrappedHeight > topViewHeightConstraintRange.lowerBound,
                scrollView.contentOffset.y > 0 {
                
                dragDirection = .Up
                innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta)
                scrollView.contentOffset.y -= delta
            }
            
            /**
             *  Re-size (Expand) the top view only when the conditions meet:-
             *  1. The current offset of the table view should be lesser than the previous offset indicating an downward scroll.
             *  2. Optional - The top view's height should be within its maximum height. Skipping this step will give a bouncy effect. Note that you need to write extra code in the outer view controller to bring back the view to the maximum possible height.
             *  3. Expand the header view only when the table view's edge is below the header view, else the table view should first scroll till it's offset is 0 and only then the header should expand.
             */
            
            if delta < 0,
                // topViewUnwrappedHeight < topViewHeightConstraintRange.upperBound,
                scrollView.contentOffset.y < 0 {
                
                dragDirection = .Down
                innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta)
                scrollView.contentOffset.y -= delta
            }
        }
        
        oldContentOffset = scrollView.contentOffset
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        //You should not bring the view down until the table view has scrolled down to it's top most cell.
        
        if scrollView.contentOffset.y <= 0 {
            
            innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //You should not bring the view down until the table view has scrolled down to it's top most cell.
        
        if decelerate == false && scrollView.contentOffset.y <= 0 {
            
            innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
        }
    }
    
}
