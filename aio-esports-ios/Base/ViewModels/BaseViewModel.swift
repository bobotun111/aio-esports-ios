//
//  BaseViewModel.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import Foundation
import RxSwift
import RxCocoa
import Combine


class BaseViewModel {
    let disposableBag = DisposeBag()
    
    var bindings = Set<AnyCancellable>()
    
    var viewController : BaseViewController?
    var pagerViewController : BasePagerViewController?
    let errorPublisher = PassthroughSubject<Error,Never>()
    
    let showErrorMessagePublisher = CurrentValueSubject<String?,Never>(nil)
    let loadingPublisher = CurrentValueSubject<Bool,Never>(false)
    var isGifLoadingView = false
    let isNoDataPublisher = PassthroughSubject<Bool,Never>()
    let isNoInternetPublisher = PassthroughSubject<Bool,Never>()
    let isSeverErrorPublisher = PassthroughSubject<Bool,Never>()
    let isEmptyCategoryListPublisher = PassthroughSubject<Bool,Never>()

    var isShowNoDataPageForUnKnownError: Bool = true
    
//    let baseUserModel = UserModel.shared
//    let notificationModel = NotificationModel.shared
    
    var updatedRefreshTokenPublisher = PassthroughSubject<BaseResponse?,Never>()
    init() {
        
    }
    deinit {
        debugPrint("Deinit \(type(of: self))")
    }
    func bindViewModel(in viewController: BaseViewController? = nil,
                       inPager pagerViewController : BasePagerViewController? = nil,
                       isDataShowingPage: Bool = true) {
        self.viewController = viewController
        self.pagerViewController = pagerViewController
        self.isShowNoDataPageForUnKnownError = isDataShowingPage
        
        loadingPublisher.sink { (result) in
            if result {
                viewController?.showLoading()
                pagerViewController?.showLoading()
            } else {
                viewController?.hideLoading()
                pagerViewController?.hideLoading()
                if let vc = viewController as? BaseTableViewController{
                    vc.hidePullToRefreshAnimationViewPublishRelay.accept(true)
                    vc.hideFooterLoadingViewPublishRealy.accept(true)
                }
            }
        
        }.store(in: &bindings)
        
        errorPublisher.sink { (error) in
            viewController?.hideLoading()
            pagerViewController?.hideLoading()
            
            if let vc = viewController as? BaseTableViewController{
                vc.hidePullToRefreshAnimationViewPublishRelay.accept(true)
                vc.hideFooterLoadingViewPublishRealy.accept(true)
            }
            
            if let error = error as? ErrorType {
                switch error {
                case .NoInterntError:
                    self.isNoInternetPublisher.send(true)
                case .KnownError(let message):
                    viewController?.showToast(message: message)
                    pagerViewController?.showToast(message: message)
                case .UnKnownError:
                    if self.isShowNoDataPageForUnKnownError {
                        self.isSeverErrorPublisher.send(true)
                    }else {
                        viewController?.showToast(message: LocalizedKey.ToastAndPickerMsg.smthWentWrong.localized())
                        pagerViewController?.showToast(message: LocalizedKey.ToastAndPickerMsg.smthWentWrong.localized())
                    }
                case .TokenExpireError(let code):
                    if code == 40101 || code == 40102 || code == 401{
                        //UserDefaultModel.clearSession()
                        //AppScreens.BaseVC.navigateToLoginVC().show()
                    }
                    else {
                        print("Token Can't be refresh \(code)")
                    }
                default:
                    self.isSeverErrorPublisher.send(true)
                }
            }
        }.store(in: &bindings)
        
        showErrorMessagePublisher.sink { (errorMessage) in
            if let message = errorMessage {
                viewController?.showToast(message: message, isShowing: {
                    self.viewController?.view.isUserInteractionEnabled = false
                }, completion: {
                    self.viewController?.view.isUserInteractionEnabled = true
                })
                
                pagerViewController?.showToast(message: message, isShowing: {
                    self.pagerViewController?.view.isUserInteractionEnabled = false
                }, completion: {
                    self.pagerViewController?.view.isUserInteractionEnabled = true
                })
            }
        }.store(in: &bindings)
    }
    
    
    func regularErrorHandling(){
        
        isNoDataPublisher.sink{
            self.viewController?.isShowNoDataAndInternet(isShow: $0)
            self.pagerViewController?.isShowNoDataAndInternet(isShow: $0)
            self.pagerViewController?.buttonBarView.isHidden = $0
        }.store(in: &bindings)
        
        isNoInternetPublisher.sink{
            self.viewController?.isShowNoDataAndInternet(isShow: $0)
            self.pagerViewController?.isShowNoDataAndInternet(isShow: $0)
            self.pagerViewController?.buttonBarView.isHidden = $0
        }.store(in: &bindings)
        
        isSeverErrorPublisher.sink{
            self.viewController?.isShowNoDataAndInternet(isShow: $0, isServerError: true)
            self.pagerViewController?.isShowNoDataAndInternet(isShow: $0, isServerError: true)
            self.pagerViewController?.buttonBarView.isHidden = $0
        }.store(in: &bindings)
    }
}
