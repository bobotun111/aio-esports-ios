//
//  LocalizedKey.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import Foundation

extension String {
    
    var localized : String {
        guard let selectedBundle = LocalizationManager.sharedInstance.getCurrentBundle() else {
            return self
        }
        return NSLocalizedString(self, bundle: selectedBundle, comment: "")
    }
}

protocol Localization {
    var rawKey : String {get}
}

extension Localization {
    func localized() -> String {
        return rawKey.localized
    }
}

struct LocalizedKey {
    
    enum Language: String, Localization {
        case english = "english"
        case burmese = "burmese"
        
        case navTitle = "nav_language"
        
        var rawKey: String {
            return self.rawValue
        }
    }
    
    enum ToastAndPickerMsg: String, Localization {
        case noInternet = "no_internet"
        case done = "done"
        case ok = "ok"
        case cancel = "cancel"
        case noData = "no_data"
        case tryAgain = "try_again"
        case delete = "action_sheet_delete"
        case edit = "action_sheet_edit"
        case couldNotConnect = "could_not_connect"
        case smthWentWrong = "smth_went_wrong"
        case comingSoon = "coming_soon"
        
        /* Toasts and confirm messages */
        case cmtDeleteFail = "cmt_delete_fail"
        case cmtDeleteSuccess = "cmt_delete_success"
        case cmtEditSuccess = "cmt_edit_success"
        case postDeleteConfirm = "post_delete_confrim"
        case deleteConfirm = "delete_confirm"
        case saveFail = "save_fail"
        case savedPhoto = "saved_photo"
        case exceededPhotoPicked = "exceeded_photo_pikced"
        
        /* picker */
        case chooseProductStatus = "choose_product_status"
        case saveSuccess = "save_success"
        case deleteSuccess = "delete_success"
        
        var rawKey: String {
            return self.rawValue
        }
    }
}
