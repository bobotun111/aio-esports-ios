//
//  AlertManager.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import UIKit

class AlertManager {
    class func showAlert(_ title: String?,
                         message: String?,
                         confirmTitle : String? = nil,
                         confirmAction: (() -> Void)? = nil,
                         cancelButtonTitle: String? = nil,
                         cancelAction: (() -> Void)? = nil,
                         inViewController viewController: UIViewController?) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let confirmAction = confirmAction {
            let alertConfirmAction = UIAlertAction(title: confirmTitle, style: .default) { (action) in
                confirmAction()
            }
            alertController.addAction(alertConfirmAction)
        }
        
        if let cancelButtonTitle = cancelButtonTitle {
            let alertCancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { (action) in
                cancelAction?()
            }
            alertController.addAction(alertCancelAction)
        }
        
        DispatchQueue.main.async {
            alertController.modalPresentationStyle = .fullScreen
            viewController?.present(alertController, animated: true, completion: nil)
        }
    }
        
    class func showCommonError(title : String = "Warning!", message : String, vc : BaseViewController) {
           showAlert(title, message: message, cancelButtonTitle: LocalizedKey.ToastAndPickerMsg.ok.localized(), inViewController: vc)
       }
    
    class func showInternetConnectionError(title : String = "Warning!", message : String, vc : BaseViewController) {
        //showAlert(title, message: message, cancelButtonTitle: "Reload",cancelAction: {vc.reloadScreen()}, inViewController: vc)
        showAlert(title, message: message, cancelButtonTitle: LocalizedKey.ToastAndPickerMsg.ok.localized(),cancelAction: nil, inViewController: vc)
    }
    
    class func showInternetConnectionErrorinSplash(title : String = "Warning!", message : String, vc : BaseViewController) {
        showAlert(title, message: message, cancelButtonTitle: "Reload",cancelAction: {vc.reloadScreen()}, inViewController: vc)
        
    }
    
    class func showCommonMessageWithAction(_ title : String = "" , message : String, actionButtonTitle : String, cancelAction: (() -> Void)? = nil, vc : BaseViewController) {
        showAlert(title, message: message, cancelButtonTitle: actionButtonTitle, cancelAction: cancelAction, inViewController: vc)
    }
    
}
