//
//  UIFont + Extension.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import UIKit

extension UIFont {

    class var navTitleFont: UIFont {
        return UIFont.dmsansBoldFont(size: 16.0,autoAjust: false)
    }
    
    class func dmsansFont(size: CGFloat, autoAjust: Bool = true) -> UIFont {
        return UIFont.screenAdjustedAppFont(name: "DMSans-Bold", size: size, autoAjust: autoAjust)
    }
    
    class func dmsansBoldItalicFont(size: CGFloat, autoAjust: Bool = true) -> UIFont {
        return UIFont.screenAdjustedAppFont(name: "DMSans-BoldItalic", size: size, autoAjust: autoAjust)
    }
    
    class func dmsansBoldFont(size: CGFloat, autoAjust: Bool = true) -> UIFont {
        return UIFont.screenAdjustedAppFont(name: "DMSans-Bold", size: size, autoAjust: autoAjust)
    }
    
    class func dmsansMediumFont(size: CGFloat, autoAjust: Bool = true) -> UIFont {
        return UIFont.screenAdjustedAppFont(name: "DMSans-Medium", size: size, autoAjust: autoAjust)
    }
    
    class func dmsansMediumItalicFont(size: CGFloat, autoAjust: Bool = true) -> UIFont {
        return UIFont.screenAdjustedAppFont(name: "DMSans-MediumItalic", size: size, autoAjust: autoAjust)
    }
    
    class func dmsansItalicFont(size: CGFloat, autoAjust: Bool = true) -> UIFont {
        return UIFont.screenAdjustedAppFont(name: "DMSans-Italic", size: size, autoAjust: autoAjust)
    }
    
    class func screenAdjustedAppFont(name: String, size: CGFloat, autoAjust : Bool) -> UIFont {
        
        if !autoAjust { return UIFont(name: name, size: size)! }
        
        /* 1.4 ratio font for tablet ipad sizes**/
        if iOSDeviceSizes.tabletSize.getBool() {
            return UIFont(name: name, size: size * 1.4)!
        }else if iOSDeviceSizes.plusSize.getBool() {
            /* 1.2 ratio font for iphone plus sizes**/
            return UIFont(name: name, size: size * 1.2)!
        }else if iOSDeviceSizes.miniSize.getBool() {
            /* 0.9 ratio font for iphone SE and mini sizes**/
            return UIFont(name: name, size: size * 0.9)!
        }
        
            return UIFont(name: name, size: size)!
    }
    
}



