//
//  ErrorHandlerView.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import UIKit

protocol ErrorHandlerDelegate {
    func didTapRetry()
}
class ErrorHandlerView: BaseView {
    
    @IBOutlet weak var imgError: UIImageView!
    @IBOutlet weak var lblErrorTitle: UILabel!
    @IBOutlet weak var lblErrorDesc: UILabel!
    @IBOutlet weak var btnRetry: RoundedCornerUIButton!
    @IBOutlet weak var btnRetryHeightConstraint: NSLayoutConstraint!
    var delegate : ErrorHandlerDelegate?
    
    override func setupUI() {
        super.setupUI()
        btnRetry.setTitle(LocalizedKey.ToastAndPickerMsg.tryAgain.localized(), for: .normal)
        lblErrorTitle.font = .dmsansBoldFont(size: 18.0)
        lblErrorDesc.font = .dmsansFont(size: 16.0)
    }
    
    func setupView(isShow : Bool , isServerError : Bool = false , errorImage : UIImage? = nil , errorTitle : String? = nil , errorDesc : String? = nil) {
        
        var error_image : UIImage?
        var error_title : String?
        var error_desc : String?
        var isInternetAvailable = Bool()
        
        ApiClient.checkReachable(success: {
            isInternetAvailable = true
            if let image = errorImage,
               let title = errorTitle,
               let desc = errorDesc{
                
                error_image = image
                error_title = title
                error_desc = desc
            }else {
                error_image = #imageLiteral(resourceName: "ic_no_data")
                error_title = LocalizedKey.ToastAndPickerMsg.noData.localized()
                error_desc = ""
            }
            
        }) {
            isInternetAvailable = false
            error_image = #imageLiteral(resourceName: "ic_no_internet")
            error_title = LocalizedKey.ToastAndPickerMsg.couldNotConnect.localized()
            error_desc = LocalizedKey.ToastAndPickerMsg.noInternet.localized()
        }
        
        if isServerError {
            error_image = #imageLiteral(resourceName: "ic_server_error")
            error_title = LocalizedKey.ToastAndPickerMsg.smthWentWrong.localized()
            isInternetAvailable = false
            error_desc = ""
        }
        
        imgError.image = error_image
        lblErrorTitle.text = error_title
        lblErrorDesc.text = error_desc
        btnRetry.isHidden = isInternetAvailable
        btnRetryHeightConstraint.constant = isInternetAvailable ? 0.0 : 50.0
        btnRetry.isUserInteractionEnabled = true
        
        
        self.layoutIfNeeded()
        self.layoutSubviews()
        self.setNeedsLayout()
    }
    
    func setupViewForOnlyDesc(isShow : Bool , errorDesc : String? = nil) {
        btnRetry.isHidden = true
        btnRetryHeightConstraint.constant = 0
        imgError.isHidden = true
        lblErrorTitle.isHidden = true
        
        lblErrorDesc.text = errorDesc
        
        self.layoutIfNeeded()
        self.layoutSubviews()
        self.setNeedsLayout()
    }
    
    func removeView() {
        self.removeFromSuperview()
        
    }
    
    @IBAction func onClickRetry(_ sender: Any) {
        if ApiClient.checkReachable() {
            self.removeView()
            delegate?.didTapRetry()
        }
        
    }
}

