//
//  AppScreens.swift
//  aio-esports-ios
//
//  Created by Thinzar Soe on 5/1/22.
//

import UIKit

struct AppScreens {
    
    static var shared = AppScreens()
    
    var startAnimationView : UIView?
    var currentVC : UIViewController?
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
}
